Feature: My sample API Testing Feature


  @SampleScenarioOutlineGetAct
  Scenario Outline: First Scenario
    When I hit the url of an API for "GetMethod"
    And I enter the value number "<value>"
    Then I expect a Response Code as <responsecode>
    Examples:
      | value    | responsecode |
      | 1        | 200          |
      | 18322331 | 404          |
      | 18       | 200          |

  @SampleScenarioGetAct
  Scenario: getting a post request
    When I hit the url of an API for "PostMethod"
    And I enter
      """
       {
  "id": 1112,
  "category": {
    "id": 0,
    "name": "string"
  },
  "name": "hectic",
  "photoUrls": [
    "string"
  ],
  "tags": [
    {
      "id": 0,
      "name": "string"
    }
  ],
  "status": "available"
}

      """
    Then I should be able to post it successfully to get status code 200

  @SampleScenarioGetAct
  Scenario: Using the datatables
    When I hit the url of an API for "GetMethod"
    And I enter the value and expect response Code
      | 1  | 200 |
      | 18 | 200 |


  @SampleScenarioOutline_Post_Delete_Json_Act
  Scenario Outline:Using  examples to take data
    Then I hit the url of an API for "PostMethod"
    And I enter
   """
   {
  "id":<id> ,
  "category": {
    "id": <cid>,
    "name": "<cname>"
  },
  "name": "<name>",
  "photoUrls": [
    "<purls>"
  ],
  "tags": [
    {
      "id": <tid>,
      "name": "<tname>"
    }
  ],
  "status": "<status>"
}
"""
    Then I should be able to post it successfully to get status code <PostCode>
    Then I should be able to delete <id> successfully to get status code <DeleteCode>
    Examples:
      | id  | cid  | cname | name    | purls      | tid    | tname   | status    | PostCode | DeleteCode |
      | 231 | 3453 | rsdv  | rffewf  | sffggfgkjg | 23423  | efwfwef | available | 200      | 200        |
      | 443 | 3454 | sewf  | wefwfef | fgdfbd     | 234234 | efwsfwf | available | 200      | 200        |


