package StepDefinition;

import Utility.StepData;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class DeleteStep {

    static RequestSpecification requestSpecification= StepData.stepData.getRequestSpecification();
    static Response response;

    @Then("^I should be able to delete (\\d+) successfully to get status code (\\d+)$")
    public void iShouldBeAbleToDeleteItSuccessfullyToGetStatusCode(int id,int code) throws Throwable {
        response=requestSpecification.delete("/v2/pet/"+id);
        response.prettyPrint();
        System.out.println(response.getStatusCode());
        Assert.assertEquals(code,response.getStatusCode());
    }
}
