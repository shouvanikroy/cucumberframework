package StepDefinition;

import Utility.StepData;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import io.restassured.response.Response;
import java.io.IOException;

public class Steps {

    static RequestSpecification requestSpecification;
    static Response response;


    @When("^I hit the url of an API for \"([^\"]*)\"$")
    public void iHitTheUrlOfAnAPIFor(String id) throws Exception {

        StepData.stepData = new StepData();
        for (int i = 0; i < StepData.stepData.getProperties().getRepositories().size(); i++) {
            if (StepData.stepData.getProperties().getRepositories().get(i).getId().equals(id)) {
                StepData.stepData.setUri(StepData.stepData.getProperties().getRepositories().get(i).getUrl());
            }
        }
    }
    @And("^I enter the value number \"([^\"]*)\"$")
    public void iEnterTheValueNumber(String arg0)
    {
        StepData.stepData.setUri(StepData.stepData.getUri()+"/"+arg0);
        System.out.println(StepData.stepData.getUri());
       /* RestAssured.baseURI=StepData.stepData.getUri();*/
        StepData.stepData.setRequestSpecification(RestAssured.given());
        Response response=StepData.stepData.getRequestSpecification().get(StepData.stepData.getUri());
        StepData.stepData.setResponse(response);
        response.body().prettyPrint();

    }
}

