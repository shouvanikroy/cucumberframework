package StepDefinition;

import Utility.StepData;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;

public class ResponseSteps {

    @Then("^I expect a Response Code as (\\d+)$")
    public void i_expect_a_Response_Code_as(int arg1) throws Exception {
        System.out.println("-------------------Output should be"+arg1);
        Assert.assertEquals( arg1,StepData.stepData.getResponse().getStatusCode());
    }



    @And("^I enter the value and expect response Code$")
    public void iEnterTheValueAndExpectResponseCode(DataTable dataTable)
    {

        int count=0;
        Response response=null;
        System.out.println("--------------"+StepData.stepData.getUri()+"---------------------");
        RestAssured.baseURI=StepData.stepData.getUri();
        StepData.stepData.setRequestSpecification(RestAssured.given());
        List<List<String>> values=dataTable.raw();
        for (int i=0;i<values.size();i++) {
            response = StepData.stepData.getRequestSpecification().request(Method.GET, "/" + values.get(i).get(0));
            StepData.stepData.setResponse(response);
            if (StepData.stepData.getResponse().getStatusCode() == Integer.parseInt(values.get(i).get(1)))
                count++;
        }

        Assert.assertEquals(count,values.size());
    }

    @And("^I enter the value and expect response Code with data in excel sheet \"([^\"]*)\"$")
    public void iEnterTheValueAndExpectResponseCodeWithDataInExcelSheet(String path)  {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}


