package StepDefinition;

import Utility.StepData;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import gherkin.deps.com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.junit.Assert;

public class PostSteps {
    static RequestSpecification requestSpecification;
    static Response response;

    @And("^I enter$")
    public void iEnter(String json) throws Exception {

        JSONObject jsonObject= new Gson().fromJson(json,JSONObject.class);

        json=jsonObject.toJSONString();
        System.out.println(json);

        RestAssured.baseURI= StepData.stepData.getUri();
        requestSpecification= RestAssured.given();
        requestSpecification.header("Content-Type", "application/json");
        requestSpecification.body(json);
        StepData.stepData.setRequestSpecification(requestSpecification);


    }
    @Then("^I should be able to post it successfully to get status code (\\d+)$")
    public void iShouldBeAbleToPostItSuccessfullyToGetStatusCode(int code){
        response=requestSpecification.post("/v2/pet");
        StepData.stepData.setResponse(response);
        response.prettyPrint();
        System.out.println(response.getStatusCode());
        Assert.assertEquals(code,response.getStatusCode());
    }
}


