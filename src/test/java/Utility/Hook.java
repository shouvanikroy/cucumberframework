package Utility;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hook{

    @Before
    public void BeforeFun()
    {
        System.out.println("Before the test Case");
    }

    @After
    public void AfterFun()throws Exception
    {
        System.out.println("After the test case");

    }
}