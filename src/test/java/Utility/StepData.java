package Utility;


import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.io.FileReader;
import java.io.IOException;

public class StepData {

    protected Response response;
    protected ValidatableResponse validatableResponse;
    protected RequestSpecification requestSpecification;
    protected String uri;
    protected Model properties;
    public static StepData stepData;


//To get the properties
    public Model getProperties()
    {
        return properties;
    }


    //Reading URL from the pom file
    public StepData() throws XmlPullParserException, IOException
    {
        MavenXpp3Reader reader=new MavenXpp3Reader();
        Model properties=reader.read(new FileReader("pom.xml"));
        this.properties=properties;
    }


    //getting URI
    public String getUri()
    {
        return uri;
    }


//Setting URI
    public void setUri(String uri)
    {
        this.uri = uri;
    }


 //Get RequestSpecification Class
    public RequestSpecification getRequestSpecification()
    {
        return requestSpecification;
    }


//Set ResponseSpecification
    public void setRequestSpecification(RequestSpecification requestSpecification)
    {
        this.requestSpecification = requestSpecification;
    }


//Get Response Class
    public Response getResponse()
    {
        return response;
    }


//Set Response Class
    public void setResponse(Response response)
    {
        this.response = response;
    }

}
