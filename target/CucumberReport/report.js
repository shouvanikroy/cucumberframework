$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("SampleFeature.feature");
formatter.feature({
  "name": "My sample API Testing Feature",
  "description": "  As a Automation Tester",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SampleScenarioOutlineGetAct"
    }
  ]
});
formatter.step({
  "name": "I hit the url of an API for \"GetMethod\"",
  "keyword": "When "
});
formatter.step({
  "name": "I enter the city name \"\u003ccity\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I expect a Response Code as \u003cresponsecode\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "city",
        "responsecode"
      ]
    },
    {
      "cells": [
        "Jamshedpur",
        "200"
      ]
    },
    {
      "cells": [
        "ABCD",
        "400"
      ]
    },
    {
      "cells": [
        "Hyderabad",
        "200"
      ]
    }
  ]
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SampleScenarioOutlineGetAct"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I hit the url of an API for \"GetMethod\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.i_hit_the_url_of_an_API_for(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the city name \"Jamshedpur\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.iEnterTheCityName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect a Response Code as 200",
  "keyword": "Then "
});
formatter.match({
  "location": "ResponseSteps.i_expect_a_Response_Code_as(int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SampleScenarioOutlineGetAct"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I hit the url of an API for \"GetMethod\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.i_hit_the_url_of_an_API_for(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the city name \"ABCD\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.iEnterTheCityName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect a Response Code as 400",
  "keyword": "Then "
});
formatter.match({
  "location": "ResponseSteps.i_expect_a_Response_Code_as(int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SampleScenarioOutlineGetAct"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I hit the url of an API for \"GetMethod\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.i_hit_the_url_of_an_API_for(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the city name \"Hyderabad\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.iEnterTheCityName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect a Response Code as 200",
  "keyword": "Then "
});
formatter.match({
  "location": "ResponseSteps.i_expect_a_Response_Code_as(int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SampleScenarioGetAct"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I hit the url of an API for \"GetMethod\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.i_hit_the_url_of_an_API_for(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the city name and expect response Code",
  "rows": [
    {
      "cells": [
        "Jamshedpur",
        "200"
      ]
    },
    {
      "cells": [
        "Chennai",
        "200"
      ]
    },
    {
      "cells": [
        "Hyderabad",
        "200"
      ]
    },
    {
      "cells": [
        "Village",
        "200"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "ResponseSteps.iEnterTheCityNameAndExpectResponseCode(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SampleScenario_Post_Json_Act"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I hit the url of an API for \"PostMethod\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.i_hit_the_url_of_an_API_for(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the First Name the Last Name with username and password and the email ID",
  "keyword": "And ",
  "doc_string": {
    "value": "{\n\"FirstName\": \"AnkitaSingh\",\n\"LastName\": \"Singh\",\n\"UserName\": \"AnkitaSingh\",\n\"Password\": \"ankita@\",\n\"Email\": \"AnkitaSingh@gmail.com\"\n}"
  }
});
formatter.match({
  "location": "PostSteps.i_enter_the_First_Name_the_Last_Name_the_email_ID(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should be able to post it successfully to get status code 201",
  "keyword": "Then "
});
formatter.match({
  "location": "PostSteps.i_should_be_able_to_post_it_successfully_to_get_status_code(int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SampleScenarioOutline_Post_Delete_Json_Act"
    }
  ]
});
formatter.step({
  "name": "I hit the url of an API for \"PostMethod\"",
  "keyword": "Then "
});
formatter.step({
  "name": "I enter the First Name the Last Name with username and password and the email ID",
  "keyword": "And ",
  "doc_string": {
    "value": "{\n\"FirstName\": \"\u003cFirst\u003e\",\n\"LastName\": \"\u003cLast\u003e\",\n\"UserName\": \"\u003cUsr\u003e\",\n\"Password\": \"\u003cPass\u003e\",\n\"Email\": \"\u003cEmail\u003e\"\n}"
  }
});
formatter.step({
  "name": "I should be able to post it successfully to get status code \u003cPostCode\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "I should be able to delete it successfully to get status code \u003cDeleteCode\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "First",
        "Last",
        "Usr",
        "Pass",
        "Email",
        "PostCode",
        "DeleteCode"
      ]
    },
    {
      "cells": [
        "Raj",
        "Anand",
        "r",
        "r",
        "rajs@mindfire.com",
        "201",
        "200"
      ]
    },
    {
      "cells": [
        "Shaswat",
        "Swaroop",
        "shasp",
        "shas",
        "shas@mindfire.com",
        "201",
        "200"
      ]
    }
  ]
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SampleScenarioOutline_Post_Delete_Json_Act"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I hit the url of an API for \"PostMethod\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.i_hit_the_url_of_an_API_for(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the First Name the Last Name with username and password and the email ID",
  "keyword": "And ",
  "doc_string": {
    "value": "{\n\"FirstName\": \"Raj\",\n\"LastName\": \"Anand\",\n\"UserName\": \"r\",\n\"Password\": \"r\",\n\"Email\": \"rajs@mindfire.com\"\n}"
  }
});
formatter.match({
  "location": "PostSteps.i_enter_the_First_Name_the_Last_Name_the_email_ID(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should be able to post it successfully to get status code 201",
  "keyword": "Then "
});
formatter.match({
  "location": "PostSteps.i_should_be_able_to_post_it_successfully_to_get_status_code(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should be able to delete it successfully to get status code 200",
  "keyword": "Then "
});
formatter.match({
  "location": "DeleteStep.iShouldBeAbleToDeleteItSuccessfullyToGetStatusCode(int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SampleScenarioOutline_Post_Delete_Json_Act"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I hit the url of an API for \"PostMethod\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.i_hit_the_url_of_an_API_for(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the First Name the Last Name with username and password and the email ID",
  "keyword": "And ",
  "doc_string": {
    "value": "{\n\"FirstName\": \"Shaswat\",\n\"LastName\": \"Swaroop\",\n\"UserName\": \"shasp\",\n\"Password\": \"shas\",\n\"Email\": \"shas@mindfire.com\"\n}"
  }
});
formatter.match({
  "location": "PostSteps.i_enter_the_First_Name_the_Last_Name_the_email_ID(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should be able to post it successfully to get status code 201",
  "keyword": "Then "
});
formatter.match({
  "location": "PostSteps.i_should_be_able_to_post_it_successfully_to_get_status_code(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should be able to delete it successfully to get status code 200",
  "keyword": "Then "
});
formatter.match({
  "location": "DeleteStep.iShouldBeAbleToDeleteItSuccessfullyToGetStatusCode(int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});